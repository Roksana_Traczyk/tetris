import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Roksana on 27.04.2017.
 */
public class Board extends JPanel implements ActionListener{
    public static final int borderWidth = 5;
    public static final int columnsCount = 10;
    public static final int visibleRowsCount = 20;
    public static final int hiddenRowsCount = 2;
    public static final int rowsCount = visibleRowsCount;// + hiddenRowsCount;
    public static final int blockSize = 24;
    public static  int LOCATION = 24*3;

    final static short width = blockSize*columnsCount; //240
    final static short height = blockSize*rowsCount;    //528 //480

    private Shape shape = Shape.ShapeNo  ;
    private int shapeRotation=1;
    private Random rand = new Random();
    public static  boolean gameOver = false;
    private static boolean isFallingFinished = false ;
    private static boolean isStarted = false;
    public static boolean isPaused = false;
    Timer timer;
    public static int timerTime=400;

    public static int numLinesRemoved = 0;
    public static int level = 1;
    public static int points = 0;
    public static int pointsLevel = 1;
    public static int countLinesRemoved=0;
    public static int pointsLinesRemoved=0;

    private int [][] gameTable = new int[rowsCount+1][columnsCount+2];
    private int [][] bufGameTable = new int[rowsCount+1][columnsCount+2];
    private static  int curY = 0;
    private static int curX = LOCATION;

    private int curRow ;
    private int curCol ;
    public  NamePlayer namePlayer;
    public Tetris game;
   // public static  ChangingPointsLabel changingPointsLabel = new ChangingPointsLabel();

    private int caseShape;
    private static int usedHeight=0;
    public static int previousPoints = 0;




    Board(){
        super();
       // setPreferredSize(new Dimension(width,height));
        setFocusable(true);
        setPreferredSize(new Dimension(250,580));
        setBackground(Color.black);
        timer = new Timer(timerTime,this);
        timer.start();
        setCurY(-48);
        setCurX(LOCATION);
        setGameTable(gameTable);
        addKeyListener(new TAdapter());





        /*okno.setForeground(new Color(155,32,112));
        okno.setFont(okno.getFont().deriveFont(25.0f));
        add(okno,BorderLayout.EAST);*/


    }

    public void clearEverything()
    {
        gameOver = false;
        isFallingFinished = false ;
        isStarted = false;
        isPaused = false;
        numLinesRemoved = 0;
        level = 1;
        points = 0;
        pointsLevel = 1;
        countLinesRemoved=0;
        pointsLinesRemoved=0;
        curY = 0;
        curX = LOCATION;
        updateBoard();

        setCurY(-48);
        setCurX(LOCATION);
        setGameTable(gameTable);
        timer.start();

    }
    public void drawGrid(Color colorBlock,Color colorEdge,int x, int y, Graphics g)
    {
        g.setColor(colorEdge);
        for (int i=x;i<240;i+=blockSize)
        {
            for(int j=y;j<480/*528*/;j+=blockSize)
            {
                g.drawRect(i,j,blockSize,blockSize);
            }
        }

    }
    public void nextShape()
    {
        this.shape = shape.randShape();
        this.shapeRotation = rand.nextInt(3);
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Color colorEdge = new Color(95,96,97);
        drawGrid(Color.black, colorEdge,0,0,g);
        drawShape(g,shapeRotation,curX,curY,shape);
        g.setColor(Color.lightGray);
        g.drawRect(0 , 0,width,height);
        drawUpdateBoard(g);
        if(NamePlayer.namePlayerIsClose == true)
        {
            nextShape();
            NamePlayer.namePlayerIsClose = false;


        }


    }

    public void drawSquare(Graphics g, int x, int y,Shape shape)
    {
        g.setColor(shape.getColor());
        g.fillRect(x,y,blockSize, blockSize);
        g.setColor(Color.black);
        g.drawRect(x,y,blockSize,blockSize);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(shape != Shape.ShapeNo)
        {
            oneLineDown();
        }
        if(this.gameOver == true)
        {
            Tetris.frame.setVisible(false);
            GameOver gameOverr = new GameOver();
            gameOverr.setVisible(true);
            this.gameOver = false;


        }

    }


    public void oneLineDown()
    {
        if(shape != Shape.ShapeNo)
        {
            setPoints(countDownPoints());
            setCountLevel(this.points);
            setCountNumLinesRemoved(this.points);

        }
        removeLines();
        //setNumLinesRemoved(0);
        curY+=blockSize;
        if(curY >= 0)
        {
            setCurRow(curY);
            setCurCol(curX);
        }
        isFallingFinish();
        if(isFallingFinished)
        {
            updateBoard();
            setCurX(LOCATION);
            setCurY(-48);
            nextShape();
            isFallingFinished = false;
            setCurRow(-1);
        }
        else
        {
            repaint();
        }
    }

    public void setGameTable(int[][] gameTable)
    {
        for(int i=0;i<rowsCount+1;i++)
        {
            for(int j=0;j<columnsCount+2;j++)
            {
                if(j == 0 || j == columnsCount+1 || i == rowsCount)
                {
                    gameTable[i][j] = -1;
                }
                else
                {
                    gameTable[i][j]= 0;
                }
            }
        }
        for(int i=0;i<rowsCount+1;i++)
        {
            for(int j=0;j<columnsCount+2;j++)
            {
                System.out.print(gameTable[i][j]+"   ");
            }
            System.out.print("\n");
        }
    }

    public void isFallingFinish()
    {
        if(curRow >= 0)
        {
            for(int i=1;i<columnsCount+1;i++)
            {
                if(
                        gameTable[curRow+shape.shapeHeight(shapeRotation)-1][i] == -1 ||
                                (gameTable[curRow+shape.shapeHeight(shapeRotation)-1][i] >= 1  &&
                                        gameTable[curRow+shape.shapeHeight(shapeRotation)-1][i] <= 7  ))

                    if(isPlace(curRow)) isFallingFinished = false;

                    else isFallingFinished = true;

            }
        }
        else if(curRow == -1)
        {
            for(int i=1;i<columnsCount+1;i++)
            {
                if(     gameTable[curRow+1+shape.shapeHeight(shapeRotation)-1][i] == -1 ||
                        gameTable[curRow+1+shape.shapeHeight(shapeRotation)-1][i] >= 1  &&
                                gameTable[curRow+1+shape.shapeHeight(shapeRotation)-1][i] <= 7  )
                {
                    isFallingFinished = true;
                }
            }

        }
    }
    public boolean isPlace(int curRow)
    {
        boolean [][][] shapeCoords = shape.getShapeCoords();

        if(curRow > 0)
        {
            for(int i = shape.getSizeX()-1; i>=0 ; i--)
            {
                for(int j=shape.getSizeY()-1; j>=0 ; j--)
                {
                    if(shapeCoords[shapeRotation][i][j] == true && (gameTable[(curRow+i)][(curCol+j)+1] == -1 || (gameTable[(curRow+i)][(curCol+j)+1] >= 1  && gameTable[(curRow+i)][(curCol+j)+1] <= 7)))
                    {
                        // isAvailablePlace=false;
                        return false;
                    }

                }
            }
        }
        return true;
    }


    public void updateBoard()
    {
        caseShape = caseShape(shape);
        boolean [][][] shapeCoords = shape.getShapeCoords();

        if(curRow >=shape.shapeHeight(shapeRotation) && curRow>=-1)
        {
            for(int i=0;i<shape.getSizeY();i++)
                for(int j=0;j<shape.getSizeX();j++)
                {
                    if(shapeCoords[shapeRotation][i][j])
                    {
                        gameTable[(curRow+i)-1][(curCol+j)+1] = caseShape;
                    }
                }
        }
        else
        {
            timer.stop();
            this.gameOver = true;
        }


        for(int i=0;i<rowsCount+1;i++)
        {
            for(int j=0;j<columnsCount+2;j++)
            {
                System.out.print(gameTable[i][j]+"   ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");


    }

    public void drawUpdateBoard(Graphics g)
    {

        for(int i = 0; i < rowsCount ; i++){
            for(int j = 1; j < columnsCount + 1 ; j++){
                for(int n=1;n<8;n++)
                {
                    if(this.gameTable[i][j] == n)// && this.gameTable[i][j] <= 7)
                    {
                        draw(g,n,i,j);
                    }
                }
            }
        }
    }

    public void draw (Graphics g,int n, int i,int j)
    {
        Color color = null;
        color = caseShapeColor(n);
        drawBoardSquare(g, ((j-1) * blockSize), (i * blockSize),color);
    }

    public void drawBoardSquare(Graphics g, int x, int y,Color color)
    {
        g.setColor(color);
        g.fillRect(x,y,blockSize, blockSize);
        g.setColor(Color.black);
        g.drawRect(x,y,blockSize,blockSize);

    }

    public int caseShape(Shape shape)
    {
        int result = 0;
        switch(shape)
        {
            case ShapeI:    result=1;
                            break;

            case ShapeJ:    result=2;
                            break;

            case ShapeL:    result=3;
                            break;

            case ShapeO:    result=4;
                            break;

            case ShapeS:    result=5;
                            break;

            case ShapeT:    result=6;
                            break;

            case ShapeZ:    result=7;
                            break;
        }
        return result;
    }

    public Color caseShapeColor(int colorNumber)
    {
        Color result = null ;
        switch(colorNumber)
        {
            case 1:
                            result= Shape.ShapeI.getColor();
                break;

            case 2:         result= Shape.ShapeJ.getColor();
                break;

            case 3:         result= Shape.ShapeL.getColor();
                break;

            case 4:         result= Shape.ShapeO.getColor();
                break;

            case 5:         result= Shape.ShapeS.getColor();
                break;

            case 6:         result= Shape.ShapeT.getColor();
                break;

            case 7:         result= Shape.ShapeZ.getColor();
                break;
        }
        return result;
    }


    public void setCurY(int curY)
    {
        this.curY = curY;
    }

    public void setCurX(int curX)
    {
        this.curX = curX;
    }

    public void setCurRow(int currentY)
    {
        this.curRow = currentY/24;
    }

    public void setCurCol(int currentX)
    {
        this.curCol = currentX/24;
    }

    public void removeLines()
    {
        int licznik=0;
        int beginHeight=countHeight();
        int finalHeight=0;
        //zrobic obliczanie roznicy wysokosci aby odpowiednio dac punkty za ilosc
        //usunietych lini

        for(int i=0; i<rowsCount;i++)
        {
            for(int j=0; j<columnsCount+1;j++)
            {
                if(gameTable[i][j]>=1 && gameTable[i][j]<=7) licznik++;

                 if(licznik==10)
                 {
                     for(int k=i;k>0;k--)
                     {
                         for(int l=0;l<columnsCount+1;l++)
                         {
                             gameTable[k][l] = gameTable[k-1][l];
                         }
                     }

                 }
            }
            licznik=0;
        }
        finalHeight=countHeight();
        setLinesPoints(beginHeight,finalHeight);
    }
    public int countHeight()
    {
        this.usedHeight=0;
        for(int i=0;i<rowsCount;i++)
        {
            for(int j=0;j<columnsCount+1;j++)
            {
                if(gameTable[i][j]>=1 && gameTable[i][j]<=7)
                {
                    this.usedHeight++;
                    break;
                }


            }
        }
        return usedHeight;
    }

    public void setLinesPoints(int beginHeight, int finalHeight)
    {
        int n=beginHeight-finalHeight;
        this.countLinesRemoved +=n;
        if(n==1)
        {
            setPoints(30);
        }
        else if (n==2)
        {
            setPoints(70);
        }
        else if (n==3)
        {
            setPoints(120);
        }
        else if (n==4)
        {
            setPoints(150);
        }
    }

    public void drawShape(Graphics g, int rotation, int x, int y, Shape shape)
    {
        boolean[][][] coords = shape.getShapeCoords();
        int sizeX= shape.getSizeX();
        int sizeY = shape.getSizeY();
        int bufX=x, bufY=y;

        for(int i=0;i<sizeX;i++)
        {
            y = (blockSize*i);

            for(int j=0;j<sizeY;j++) {
                x = (blockSize * j);

                if (coords[rotation][i][j] == true)
                {
                    drawSquare(g, x + bufX, y + bufY, shape);
                }
            }
        }
    }


    class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {


            int keycode = e.getKeyCode();

            switch (keycode) {

                case KeyEvent.VK_LEFT:
                    if(!tryMoveLeft()) return;

                    break;
                case KeyEvent.VK_RIGHT:
                    if(!tryMoveRight()) return;


                    break;
                case KeyEvent.VK_DOWN:
                    oneLineDown();

                    break;
                case KeyEvent.VK_UP:
                    rotatePiece();


                    break;
                case KeyEvent.VK_SPACE:
                    isPaused();
                    break;

            }

        }
    }

    public void isPaused()
    {
        this.isPaused = !this.isPaused;
        if(this.isPaused)
        {
            timer.stop();
        }
        else
        {
            timer.start();
        }
        repaint();
    }

    public void rotatePiece(){

        this.shapeRotation++;
        if(this.shapeRotation > 3) this.shapeRotation = 0;
        this.shape.setRotation(this.shapeRotation);
        repaint();
    }

    public boolean tryMoveLeft()
    {
        if(this.curY <0)
        {
            return false;
        }
        else if(isPlaceLeft(this.curX, this.curY))
        {
            this.curX -= blockSize ;
            repaint();
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isPlaceLeft(int curX, int curY)
    {
        setCurCol(curX);
        setCurRow(curY);
        boolean [][][] shapeCoords = shape.getShapeCoords();


            for(int j=0 ; j < shape.getSizeY() ; j++)
            {
                for(int i=0 ; i < shape.getSizeX();  i++)
                {

                    if(shapeCoords[shapeRotation][i][j] == true && (gameTable[(curRow+i)][(curCol+j)] == -1 || (gameTable[(curRow+i)][(curCol+j)] >= 1  && gameTable[(curRow+i)][(curCol+j)] <= 7)))
                    {
                        return false;
                    }
                }
            }

        return true;
    }

    public boolean tryMoveRight()
    {
        if(this.curY <0)
        {
            return false;
        }
        else if(isPlaceRight(this.curX, this.curY))
        {
            this.curX += blockSize ;
            repaint();
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isPlaceRight(int curX, int curY)
    {
        setCurCol(curX);
        setCurRow(curY);
        boolean [][][] shapeCoords = shape.getShapeCoords();


        for(int j=shape.getSizeY()-1 ; j>=0; j--)
        {
            for(int i=shape.getSizeX()-1 ; i >= 0;  i--)
            {

                if(shapeCoords[shapeRotation][i][j] == true && (gameTable[(curRow+i)][(curCol+j)+2] == -1 || (gameTable[(curRow+i)][(curCol+j)+2] >= 1  && gameTable[(curRow+i)][(curCol+j)+2] <= 7)))
                {
                    return false;
                }
            }
        }

        return true;
    }

    public int countDownPoints()
    {
        return 1;
    }

    public void setCountLevel(int points)
    {
        if(points % 500 == 0 && points != 0)
        {
            this.level++;
            setPointsLevel(this.level);
            timer.setDelay(this.timerTime-20);

        }
        else
        {
            this.level = this.pointsLevel;
        }

    }


    public void setPointsLevel(int level)
    {
        this.pointsLevel = level;
    }


    public void setCountNumLinesRemoved(int points)
    {
        /*   if(this.numLinesRemoved<=count)
        {
            this.numLinesRemoved = count;
        }
        else
        {
            this.numLinesRemoved = this.countLinesRemoved;
        }*/
        if(points - this.pointsLinesRemoved == 30)
        {
            this.numLinesRemoved += 1;
          setPointsLinesRemoved(points);
        }
        if(points - this.pointsLinesRemoved == 70)
        {
            this.numLinesRemoved += 2;
            this.pointsLinesRemoved = points;
        }
        if(points - this.pointsLinesRemoved == 150)
        {
            this.numLinesRemoved += 3;
            this.pointsLinesRemoved = points;
        }
        if(points - this.pointsLinesRemoved == 320)
        {
            this.numLinesRemoved += 4;
            this.pointsLinesRemoved = points;
        }
        else
        {
            setCountLinesRemoved(this.countLinesRemoved);
           this.numLinesRemoved = this.countLinesRemoved;
        }


        System.out.print(numLinesRemoved);
    }
    public void setCountLinesRemoved (int lines)
    {
        this.countLinesRemoved = lines;
    }
    public void setPointsLinesRemoved(int points)
    {
        this.pointsLinesRemoved = points;
    }
    public void setNumLinesRemoved(int lines)
    {
        this.numLinesRemoved += lines;


    }

    public void setPoints(int points)
    {
        this.points +=  points;
        //this.numLinesRemoved += points ;


    }

    public int getNumLinesRemoved()
    {
        return numLinesRemoved;
    }

    public int getLevel()
    {
        return level;
    }

    public int getPoints()
    {
        return points;
    }


}
