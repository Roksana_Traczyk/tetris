import java.awt.*;
import java.util.Random;

/**
 * Created by Roksana on 15.05.2017.
 */
public enum Shape {


    //Shape I
    ShapeI(new Color(52, 178, 193),4,4,4, new boolean[][][]{
                    {

                                    {true,	true,	true,	true},
                                    {false, false,	false,	false},
                                    {false,	false,	false,	false},
                                    {false,	false,	false,	false},
                    },

                    {
                                    {false,	false,	true,	false},
                                    {false,	false,	true,	false},
                                    {false,	false,	true,	false},
                                    {false,	false,	true,	false},
                    },

                    {
                                    {true,	true,	true,	true},
                                    {false,	false,	false,	false},
                                    {false,	false,	false,	false},
                                    {false,	false,	false,	false},
                    },

                    {
                                    {false,	true,	false,	false},
                                    {false,	true,	false,	false},
                                    {false,	true,	false,	false},
                                    {false,	true,	false,	false},
                    }
    }),
    //Shape J
    ShapeJ(new Color(193, 23, 83),4,3,3, new boolean[][][]{
                    {
                                    {true,	false,	false},
                                    {true,	true,	true},
                                    {false,	false,	false},
                    },

                    {
                                    {false,	true,	true},
                                    {false,	true,	false},
                                    {false,	true,	false},
                    },

                    {
                                    {true,	true,	true},
                                    {false,	false,	true},
                                    {false,	false,	false},
                    },

                    {
                                    {false,	true,	false},
                                    {false,	true,	false},
                                    {true,	true,	false},
                    }

    }),

    //Shape L
    ShapeL(new Color(26, 193, 12),4,3,3, new boolean[][][]{
                    {
                                    {false,	false,	true},
                                    {true,	true,	true},
                                    {false,	false,	false},
                    },
                    {
                                    {false,	true,	false},
                                    {false,	true,	false},
                                    {false,	true,	true},
                    },
                    {
                                    {true,	true,	true},
                                    {true,	false,	false},
                                    {false,	false,	false},
                    },
                    {
                                    {true,	true,	false},
                                    {false,	true,	false},
                                    {false,	true,	false},
                    }

    }),

    //Shape o
    ShapeO(new Color(193, 54, 180),4,2,2, new boolean[][][]{
                    {
                                    {true,	true},
                                    {true,	true},
                    },
                    {
                                    {true,	true},
                                    {true,	true},
                    },
                    {
                                    {true,	true},
                                    {true,	true},
                    },
                    {
                                    {true,	true},
                                    {true,	true},
                    }
    }),
    //Shape S
    ShapeS(new Color(36, 41, 193),4,3,3, new boolean[][][]{
                    {
                                    {false,	true,	true},
                                    {true,	true,	false},
                                    {false,	false,	false},
                    },
                    {
                                    {false,	true,	false},
                                    {false,	true,	true},
                                    {false,	false,	true},
                    },
                    {
                                    {false,	true,	true},
                                    {true,	true,	false},
                                    {false,	false,	false},
                    },
                    {
                                    {true,	false,	false},
                                    {true,	true,	false},
                                    {false,	true,	false},
                    }
    }),
    //Shape T
    ShapeT(new Color(220, 229, 80),4,3,3, new boolean[][][]{
                    {
                                    {false,	true,	false},
                                    {true,	true,	true},
                                    {false,	false,	false},
                    },
                    {
                                    {false,	true,	false},
                                    {false,	true,	true},
                                    {false,	true,	false},
                    },
                    {
                                    {true,	true,	true},
                                    {false,	true,	false},
                                    {false,	false,	false},
                    },
                    {
                                    {false,	true,	false},
                                    {true,	true,	false},
                                    {false,	true,	false},
                    }
    }),
    //Shape Z
    ShapeZ(new Color(229, 3, 0),4,3,3, new boolean[][][]{
                    {
                                    {true,	true,	false},
                                    {false,	true,	true},
                                    {false,	false,	false},
                    },
                    {
                                    {false,	false,	true},
                                    {false,	true,	true},
                                    {false,	true,	false},
                    },
                    {
                                    {true,	true,	false},
                                    {false,	true,	true},
                                    {false,	false,	false},
                    },
                    {
                                    {false,	true,	false},
                                    {true,	true,	false},
                                    {true,	false,	false},
                    }
    }),
    // no shape
    ShapeNo(new Color(218, 229, 224),4,4,4, new boolean[][][]{
                    {

                            {false, false,	false,	false},
                            {false, false,	false,	false},
                            {false,	false,	false,	false},
                            {false,	false,	false,	false},
                    },

                    {
                            {false, false,	false,	false},
                            {false, false,	false,	false},
                            {false,	false,	false,	false},
                            {false,	false,	false,	false},
                    },

                    {
                            {false, false,	false,	false},
                            {false, false,	false,	false},
                            {false,	false,	false,	false},
                            {false,	false,	false,	false},
                    },

                    {
                            {false, false,	false,	false},
                            {false, false,	false,	false},
                            {false,	false,	false,	false},
                            {false,	false,	false,	false},
                    }
    });




    private Color color;
    private int rotation;
    private int sizeX;
    private int sizeY;
    private boolean[][][] shapeCoords;

    Shape(Color color,int rotation, int sizeX,int sizeY, boolean[][][] coords)
    {
        this.color = color;
        this.rotation = rotation;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.shapeCoords = coords;
    }

    public int getSizeX() {
        return sizeX;
    }

    public Color getColor()
    {
        return color;
    }

    public int getSizeY() {
        return sizeY;
    }

    public int getRotation()
    {
        return rotation;
    }

    public boolean[][][] getShapeCoords() {
        return shapeCoords;
    }

    public static Shape randShape()
    {
        Random rand = new Random();
        Shape [] values = Shape.values();
        return values[rand.nextInt(6)];
    }

    public int shapeHeight(int rotation)
    {
        int counter = 0;
        for(int i=0;i<sizeY;i++)
        {
            for(int j=0;j<sizeX;j++)
            {
                if(shapeCoords[rotation][i][j])
                {
                    counter++;
                    break;
                }
            }
        }
          return counter;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }





}
