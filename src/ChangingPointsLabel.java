import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Created by Roksana on 23.05.2017.
 */
public class ChangingPointsLabel extends JPanel implements ActionListener
{

    public static Timer timer;
    public int points = 0;
    private JLabel allPoints;
    private Board board = new Board();
    static Color colorScore = new Color(155,32,112);


    public ChangingPointsLabel()
    {
        super();
        setPreferredSize(new Dimension(100,40));
        setBackground(Color.black);
        allPoints = new JLabel(Integer.toString(points));
        setLayout(new BorderLayout());
        timer = new Timer(200,this);
        timer.start();
        allPoints.setForeground(colorScore);
        allPoints.setFont(allPoints.getFont().deriveFont(25.0f));
        add(allPoints,BorderLayout.EAST);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        points = (board.getPoints());
        allPoints.setText(Integer.toString(points));
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
