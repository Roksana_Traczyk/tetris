import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Created by Roksana on 23.05.2017.
 */
public class ChangingLevelLabel extends JPanel implements ActionListener
{

    private Timer timer;
    public static int level = 0;
    private JLabel allLevel;
    private Board board = new Board();
    static Color colorLevel = new Color(255,105,0);


    public ChangingLevelLabel()
    {
        super();
        setPreferredSize(new Dimension(100,40));
        setBackground(Color.black);
        allLevel = new JLabel(Integer.toString(level));
        setLayout(new BorderLayout());
        timer = new Timer(190,this);
        timer.start();
        allLevel.setForeground(colorLevel);
        allLevel.setFont(allLevel.getFont().deriveFont(25.0f));
        add(allLevel,BorderLayout.EAST);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.level = (board.getLevel());
        allLevel.setText(Integer.toString(level));
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
