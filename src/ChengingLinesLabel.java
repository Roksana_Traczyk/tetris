import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Created by Roksana on 23.05.2017.
 */
public class ChengingLinesLabel extends JPanel implements ActionListener
{

    private Timer timer;
    public static int lines = 0;
    private JLabel allLines;
    private Board board = new Board();
    static Color colorLines = new Color(0,155,0);


    public ChengingLinesLabel()
    {
        super();
        setPreferredSize(new Dimension(100,40));
        setBackground(Color.black);
        allLines = new JLabel(Integer.toString(lines));
        setLayout(new BorderLayout());
        timer = new Timer(200,this);
        timer.start();
        allLines.setForeground(colorLines);
        allLines.setFont(allLines.getFont().deriveFont(25.0f));
        add(allLines,BorderLayout.EAST);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.lines = (board.getNumLinesRemoved()); // wpisac odpowiednia nazwe metody
        allLines.setText(Integer.toString(lines));
    }

    public void setLines(int lines) {
        this.lines =lines;
    }
}

