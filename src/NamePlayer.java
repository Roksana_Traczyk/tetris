import javax.swing.*;
import java.awt.event.*;

/**
 * Created by Roksana on 27.04.2017.
 */
public class NamePlayer extends JFrame implements ActionListener {
    public static final int height = 200;
    public static final int width = 300;
    public JButton play;
    public JTextField nameField;
    public JLabel desciption;
    public JLabel desciption2;
    public String name ="btak" ;
    public static boolean namePlayerIsClose = false;




    public NamePlayer() {
       // super();

        setTitle("Wprowadź nazwe:");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(550,250,width,height);
        setResizable(false);
        setLayout(null);

        desciption = new JLabel("Podaj nazwe gracza (max 7 znaków): ");
        desciption.setBounds(40,20,250,20);
        add(desciption);

        nameField = new JTextField();
        nameField.setBounds(60,50,150,20);
        nameField.addActionListener(this);
        add(nameField);

        play = new JButton("play");
        play.addActionListener(this);
        play.setBounds(60,100,150,20);
        add(play);

       // setLayout();
       setVisible(true);


}

    public void setParameters() {
        Tetris.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Tetris.frame.setSize(400,530);
        Tetris.frame.setLocationRelativeTo(null);
        Tetris.frame.setResizable(false);

        final Tetris game = new Tetris();

        Tetris.frame.add(game);

        /*Tetris.lScore = new JLabel(Integer.toString(Tetris.board.getPoints()), JLabel.RIGHT);
        Tetris.lScore.setForeground(Tetris.colorScore);
        Tetris.lScore.setBounds(270, 470, 100, 40);
        Tetris.lScore.setFont(Tetris.lScore.getFont().deriveFont(30.0f));

        game.add(Tetris.lScore);
        Tetris.lScore.setVisible(true);*/


        Tetris.lScore2 = new JLabel("POINTS", JLabel.RIGHT);
        Tetris.lScore2.setBounds(270, 430, 100, 40);
        Tetris.lScore2.setForeground(Tetris.colorScore);
        Tetris.lScore2.setFont(Tetris.lScore2.getFont().deriveFont(20.0f));

        Tetris.lScore2.setVisible(true);
        game.add(Tetris.lScore2);

        /*Tetris.lLines = new JLabel(Integer.toString(Tetris.board.getNumLinesRemoved()), JLabel.RIGHT);
        Tetris.lLines.setForeground(Tetris.colorLines);
        Tetris.lLines.setBounds(270, 370, 100, 40);
        Tetris.lLines.setFont(Tetris.lLines.getFont().deriveFont(30.0f));

        Tetris.lLines.setVisible(true);
        game.add(Tetris.lLines);*/

        Tetris.lLines2 = new JLabel("LINES", JLabel.RIGHT);
        Tetris.lLines2.setBounds(270, 340, 100, 40);
        Tetris.lLines2.setForeground(Tetris.colorLines);
        Tetris.lLines2.setFont(Tetris.lLines2.getFont().deriveFont(20.0f));

        Tetris.lLines2.setVisible(true);
        game.add(Tetris.lLines2);

       /* Tetris.lLevel = new JLabel(Integer.toString(Tetris.board.getLevel()), JLabel.RIGHT);
        Tetris.lLevel.setForeground(Tetris.colorLevel);
        Tetris.lLevel.setBounds(270, 290, 100, 40);
        Tetris.lLevel.setFont(Tetris.lLevel.getFont().deriveFont(30.0f));

        Tetris.lLevel.setVisible(true);
        game.add(Tetris.lLevel);*/

        Tetris.lLevel2 = new JLabel("LEVEL", JLabel.RIGHT);
        Tetris.lLevel2.setBounds(270, 250, 100, 40);
        Tetris.lLevel2.setForeground(Tetris.colorLevel);
        Tetris.lLevel2.setFont(Tetris.lLevel2.getFont().deriveFont(20.0f));

        Tetris.lLevel2.setVisible(true);
        game.add(Tetris.lLevel2);



        Tetris.lName = new JLabel(nameField.getText(), JLabel.RIGHT);
        Tetris.lName.setForeground(Tetris.colorName);
        Tetris.lName.setBounds(270, 190, 100, 40);
        Tetris.lName.setFont(Tetris.lName.getFont().deriveFont(20.0f));

        Tetris.lName.setVisible(true);
        game.add(Tetris.lName);

        Tetris.lName2 = new JLabel("NAME", JLabel.RIGHT);
        Tetris.lName2.setBounds(270, 160, 100, 40);
        Tetris.lName2.setForeground(Tetris.colorName);
        Tetris.lName2.setFont(Tetris.lName2.getFont().deriveFont(20.0f));

        Tetris.lName2.setVisible(true);
        game.add(Tetris.lName2);






        Tetris.board.setBounds(10,10,250,530);
        Tetris.board.setVisible(true);
        game.add(Tetris.board);


        Tetris.changingLevelLabel.setBounds(270,280,100,40);
        Tetris.changingLevelLabel.setVisible(true);
        game.add(Tetris.changingLevelLabel);

        Tetris.changingPointsLabel.setBounds(270,460,100,40);
        Tetris.changingPointsLabel.setVisible(true);
        game.add(Tetris.changingPointsLabel);

        Tetris.chengingLinesLabel.setBounds(270,370,100,40);
        Tetris.chengingLinesLabel.setVisible(true);
        game.add(Tetris.chengingLinesLabel);

        //game.add(Tetris.changingValues);
        //Tetris.next.setLocation(270,10);
        //game.add(Tetris.next);



        Tetris.frame.setVisible(true);

    }

public void close(){
        setVisible(false);
        namePlayerIsClose = true;
}






    @Override
    public void actionPerformed(ActionEvent e) {
        if((nameField.getText().length()>=1) && (nameField.getText().length()<=7) ) {
            close();
            setParameters();

        }



    }
}