import javax.swing.*;
import java.awt.event.*;
/**
 * Created by Roksana on 17.06.2017.
 */
public class GameOver extends JFrame implements ActionListener{
    public static final int height = 200;
    public static final int width = 300;

    public JButton ok;
    public JTextField nameField;
    public JLabel desciption;
    public JLabel desciption2;
    public JLabel desciption3;
    public JLabel currentResult;
    public JLabel previousResult;
   // public String tak ="btak" ;
    public Board board = new Board();

    public GameOver()
    {
        setTitle("GAMEOVER!!!");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(550,200,width,height);
        setResizable(false);
        setLayout(null);

        desciption = new JLabel("GAMEOVER!!");
        desciption.setBounds(70,20,250,20);
        desciption.setFont(desciption.getFont().deriveFont(20.0f));
        add(desciption);

        desciption2 = new JLabel("Twój obecny wynik: ");
        desciption2.setBounds(40,45,250,20);
        add(desciption2);

        currentResult = new JLabel(Integer.toString(Board.points));
        currentResult.setBounds(160,45,250,20);
        add(currentResult);

        desciption3 = new JLabel("Twój poprzedni wynik: ");
        desciption3.setBounds(40,70,250,20);
        add(desciption3);

        previousResult = new JLabel(Integer.toString(Board.previousPoints));
        previousResult.setBounds(170,70,250,20);
        add(previousResult);


        ok = new JButton("OK");
        ok.addActionListener(this);
        ok.setBounds(50,100,150,20);
        ok.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setVisible(false);
            }
        });
        add(ok);

        // setLayout();
        setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {


    }
}
